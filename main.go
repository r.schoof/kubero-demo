package main

import (
	"context"
	"fmt"
	"html/template"
	"io"
	"net/http"
	"os"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"github.com/redis/go-redis/v9"
)

var (
	rdb *redis.Client
)

type Render struct {
	template *template.Template
}

func (r *Render) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	err := r.template.ExecuteTemplate(w, name, data)
	if err != nil {
		fmt.Println("render error:", err)
	}
	return err
}

func main() {
	redisHost := "localhost:6379"
	if host := os.Getenv("REDIS_HOST"); host != "" {
		redisHost = host
	}

	e := echo.New()
	e.Logger.SetLevel(log.INFO)

	rdb = redis.NewClient(&redis.Options{
		Addr:     redisHost,
		Password: os.Getenv("REDIS_PASSWORD"),
		DB:       0, // use default DB
	})
	cmd := rdb.Ping(context.Background())
	if err := cmd.Err(); err != nil {
		e.Logger.Fatalf("could not connect to redis, host %s: %s", redisHost, err)
	}
	e.Logger.Info("connected to redis")

	r := &Render{
		template: template.Must(template.ParseGlob("index.tmpl")),
	}
	e.Renderer = r

	e.POST("/post", func(c echo.Context) error {
		var v struct {
			Value string
		}
		if err := c.Bind(&v); err != nil {
			return c.String(http.StatusInternalServerError, err.Error())
		}

		if v.Value == "" {
			return c.String(http.StatusInternalServerError, "empty value")
		}

		fields := strings.Fields(v.Value)
		cmd := rdb.Set(c.Request().Context(), fmt.Sprintf("INPUT:%s", fields[0]), "1", 0)
		if err := cmd.Err(); err != nil {
			return c.String(http.StatusInternalServerError, err.Error())
		}

		c.Logger().Infof("added %s to redis", fields[0])

		return c.String(http.StatusNoContent, "ok")
	})

	e.GET("/", func(c echo.Context) error {
		keys, err := getAllKeys(c.Request().Context())
		if err != nil {
			return c.String(http.StatusInternalServerError, fmt.Sprintf("redis error: %v", err))
		}

		var v struct {
			Keys []string
		}
		v.Keys = keys

		return c.Render(http.StatusOK, "index.tmpl", v)
	})
	e.Logger.Fatal(e.Start(":8080"))
}

func getAllKeys(ctx context.Context) ([]string, error) {
	var keys []string
	var cursor uint64
	for {
		var ckeys []string
		var err error
		ckeys, cursor, err = rdb.Scan(ctx, cursor, "INPUT:*", 0).Result()
		if err != nil {
			return nil, fmt.Errorf("could not get keys: %w", err)
		}

		for _, key := range ckeys {
			keys = append(keys, strings.TrimPrefix(key, "INPUT:"))
		}

		if cursor == 0 { // no more keys
			break
		}
	}

	return keys, nil
}
